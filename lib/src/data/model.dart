import 'package:clean_architecture/src/domain/entity.dart';

abstract class Model<EntityType extends Entity, DataSourceDataType> {
  Model<EntityType, DataSourceDataType> fromEntity(EntityType entity);

  EntityType toEntity();

  Model<EntityType, DataSourceDataType> fromDataSource(
      DataSourceDataType object);

  DataSourceDataType toDataSource();
}

abstract class ModelMap<EntityType extends Entity>
    extends Model<EntityType, Map<dynamic, dynamic>> {}
