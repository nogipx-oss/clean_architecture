import 'package:clean_architecture/clean_architecture.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ControllerNotifierData extends Equals {
  final List<String> tags;
  final bool value;

  ControllerNotifierData({this.tags, this.value,});

  List<Object> get properties => [value];

}

/// Controller class that hold a View business logic
abstract class Controller {
  ValueNotifier<ControllerNotifierData> _valueNotifier;
  bool _initialized = false;

  Controller() {
    _valueNotifier = ValueNotifier(ControllerNotifierData(value: false));
    _initialized = false;
  }

  ValueNotifier<ControllerNotifierData> get notifier => _valueNotifier;

  /// Called when the View init state is mounted
  /// This can be overrided to perform some initialization tasks
  Future<void> initialize(dynamic args) async {
    await onInitialize(args);
    _initialized = true;
    rebuild();
  }

  /// Called when the [_ViewState] is initialized
  Future onInitialize(dynamic args);

  /// To be implemented when is necessary to clean variables (ex: Streams)
  void onDispose(){}

  bool get isInitialized => _initialized == true;

  /// Use to explicitly rebuild a view from the controller.
  void rebuild([List<String> tags]) {
    setState(null, tags);
  }

  /// Call a controller method
  /// And then notify listeners the tags is the [View] tag
  void setState(VoidCallback callback, [List<String> tags]) {
    if (callback != null) {
      callback();
    }
    _valueNotifier.value = ControllerNotifierData(value: !_valueNotifier.value.value, tags: tags);
  }
}
