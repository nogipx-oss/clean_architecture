import 'package:clean_architecture/src/core/clean_architecture.dart';
import 'package:clean_architecture/src/presentation/controller.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// This class is based on the [ValueListenableBuilder].
/// The difference is that this class can nofity to listeners with
/// the given tag [String] from the [View]
class _ViewListenableBuilder extends StatefulWidget {
  final String tag;
  final ValueListenable<ControllerNotifierData> valueListenable;
  final ValueWidgetBuilder<ControllerNotifierData> builder;

  const _ViewListenableBuilder({
    Key key,
    this.tag,
    this.valueListenable,
    this.builder,
  }) : super(key: key);

  @override
  _ViewListenableBuilderState createState() => _ViewListenableBuilderState();
}

class _ViewListenableBuilderState extends State<_ViewListenableBuilder> {
  ControllerNotifierData value;

  @override
  void initState() {
    super.initState();
    value = widget.valueListenable.value;
    widget.valueListenable.addListener(_valueChanged);
  }

  @override
  void didUpdateWidget(_ViewListenableBuilder oldWidget) {
    if (oldWidget.valueListenable != widget.valueListenable) {
      oldWidget.valueListenable.removeListener(_valueChanged);
      value = widget.valueListenable.value;
      widget.valueListenable.addListener(_valueChanged);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    widget.valueListenable.removeListener(_valueChanged);
    super.dispose();
  }

  /// [setState] only if no tag is passed on the listenable or if the
  /// current listener has the tag in the tag list passed.
  void _valueChanged() {
    ControllerNotifierData newValue = widget.valueListenable.value;
    var tags = newValue.tags;

    if (tags == null || tags.isEmpty || tags.contains(widget.tag)) {
      setState(() {
        value = widget.valueListenable.value;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder(context, value, null);
  }
}

abstract class View<C extends Controller> extends StatefulWidget {
  final bool _listen;
  final String tag;

  C _controller;
  BuildContext _context;

  View({Key key, bool listen = true, this.tag})
      : _listen = listen ?? true,
        super(key: key);

  BuildContext get context => _context;

  C get controller => _controller;

  /// Return the arguments to be passed to the [Controller.onInitialize]
  dynamic initializationArgs() {
    return null;
  }

  /// The controller is passed on the [_ViewState] build
  void _setController(C controller) {
    _controller = controller;
  }

  /// The build context is passed on the [_ViewState] build
  void _setContext(BuildContext context) {
    _context = context;
  }

  /// Return true if it is listening for rebuild state
  bool get isListening => _listen == true;

  /// To be overrided in case the user needs to dispose (animations)
  void onDispose() {}

  /// The class called when the state is build
  Widget build(BuildContext context, C controller);

  @override
  _ViewState<C> createState() => _ViewState<C>();
}

class _ViewState<C extends Controller> extends State<View<C>> {
  C _controller;

  @override
  void initState() {
    _controller = CleanApplication.controller<C>('$C');
    initialize();

    super.initState();
  }

  @override
  void dispose() {
    _controller.onDispose();
    widget.onDispose();
    super.dispose();
  }

  Future<void> initialize() async {
    if (mounted) {
      await _controller.initialize(widget.initializationArgs());
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    widget._setContext(context);
    widget._setController(_controller);

    if (widget.isListening) {
      return _ViewListenableBuilder(
        tag: widget.tag,
        valueListenable: _controller.notifier,
        builder: (context, value, child) {
          return widget.build(context, _controller);
        },
      );
    }

    return widget.build(context, _controller);
  }
}

/// This view don't rebuild with the view state changes triggered by the
/// controller.
// ignore: must_be_immutable
abstract class StaticView<C extends Controller> extends View<C> {
  StaticView() : super(listen: false);
}

/// This view allows the [Animation] to a [View]
abstract class AnimatedView<C extends Controller> extends StatefulWidget {
  final bool _listen;
  final Duration duration;
  final Duration reverseDuration;
  final double value;
  final String tag;

  C _controller;
  BuildContext _context;

  AnimatedView({
    Key key,
    listen = true,
    @required this.duration,
    this.reverseDuration,
    this.value,
    this.tag,
  })  : _listen = listen ?? true,
        super(key: key);


  BuildContext get context => _context;

  C get controller => _controller;

  /// Return the arguments to be passed to the [Controller.onInitialize]
  dynamic initializationArgs() {
    return null;
  }

  /// The controller is passed on the [_ViewState] build
  void _setController(C controller) {
    _controller = controller;
  }

  /// The build context is passed on the [_ViewState] build
  void _setContext(BuildContext context) {
    _context = context;
  }

  /// Called when the [_AnimatedViewState.initState] is called
  void onInitialize(AnimationController animationController);

  /// Return true if it is listening for rebuild state
  bool get isListening => _listen == true;

  /// To be overrided in case the user needs to dispose (animations)
  void onDispose() {}

  /// The class called when the state is build
  Widget build(BuildContext context, C controller,
      AnimationController animationController);

  @override
  _AnimatedViewState<C> createState() => _AnimatedViewState<C>();
}

class _AnimatedViewState<C extends Controller> extends State<AnimatedView<C>>
    with SingleTickerProviderStateMixin {
  C controller;
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    controller = CleanApplication.controller<C>('$C');

    animationController = AnimationController(
      duration: widget.duration,
      reverseDuration: widget.reverseDuration,
      value: widget.value,
      vsync: this,
    );

    widget.onInitialize(animationController);

    initialize();
  }

  @override
  void dispose() {
    controller.onDispose();
    widget.onDispose();

    if (animationController != null) {
      animationController.dispose();
    }

    super.dispose();
  }

  Future<void> initialize() async {
    if (mounted) {
      await controller.initialize(widget.initializationArgs());
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {

    widget._setContext(context);
    widget._setController(controller);

    if (widget.isListening) {
      return _ViewListenableBuilder(
        tag: widget.tag,
        valueListenable: controller.notifier,
        builder: (context, value, child) {
          return widget.build(context, controller, animationController);
        },
      );
    }

    return widget.build(context, controller, animationController);
  }
}
