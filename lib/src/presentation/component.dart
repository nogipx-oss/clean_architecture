import 'package:clean_architecture/src/presentation/controller.dart';
import 'package:clean_architecture/src/presentation/view.dart';

/// This component will rebuild when the view state change is triggered by the
/// controller
// ignore: must_be_immutable
abstract class DynamicComponent<C extends Controller> extends View<C> {
  DynamicComponent({String tag}) : super(listen: true, tag: tag);
}

/// This component don't rebuild with the view state changes triggered by the
/// controller.
// ignore: must_be_immutable
abstract class StaticComponent<C extends Controller> extends View<C> {
  StaticComponent() : super(listen: false);
}
