import 'package:clean_architecture/clean_architecture.dart';
import 'package:flutter/foundation.dart';

/// A function to provide an instance of an interface implementation
typedef InstanceBuilder<T> = T Function();

/// Provides multiple implementations to a Contract/Interface (abstract class).
/// All interfaces are instanciated at the registration time (application init).
/// This is intended mainly for [Repository] implementations.
/// TODO: This can be improved with lazy singleton?
class Implementation<T> {
  /// This map have an instance builder multiple environment
  /// Example: {'dev': () => FakeImpl(), 'prod': () => FirebaseImpl() }
  ///
  final Map<String, InstanceBuilder<T>> instances;
  String _name;

  Implementation({
    @required this.instances,
  }) : assert(instances != null) {
    _name = '$T';
  }

  String get name => _name;
}
