import 'dart:async';

import 'package:flutter/foundation.dart';

class StreamConverter {
  /// Transform a given [Stream] of [StreamType] to a [Stream] of [ResultType]
  /// This can be used to transform a firebase [QuerySnapshot] to [List]
  ///
  static Stream<ResultType> transform<StreamType, ResultType>({
    @required Stream<StreamType> stream,
    @required ResultType Function(StreamType) converter,
  }) {
    assert(stream != null);
    assert(converter != null);

    return stream
        .transform(StreamTransformer.fromHandlers(handleData: (value, sink) {
      sink.add(converter(value));
    }));
  }
}
