import 'package:clean_architecture/clean_architecture.dart';
import 'package:clean_architecture/src/core/factory.dart';
import 'package:clean_architecture/src/core/service.dart';
import 'package:clean_architecture/src/domain/repository.dart';
import 'package:clean_architecture/src/domain/usecase.dart';
import 'package:clean_architecture/src/core/implementation.dart';
import 'package:flutter/widgets.dart';
import 'package:loggable_mixin/loggable_mixin.dart';

/// This class provides the dependencies to be injected in the application
abstract class Configuration extends InheritedWidget with Loggable {
  final Widget child;

  Configuration({@required this.child}) : assert(() {
    if (child == null) {
      log.e("Specify child. It will be included in widget tree.");
      return false;
    }
    return true;
  }());

  /// If true all the Routes will be wrapped with a safe area
  bool wrapViewWithSafeArea() => true;

  /// Repositories
  List<Implementation<Repository>> repositories();

  /// Use cases
  List<UseCase> usecases();

  /// Controllers for each view
  List<Factory<Controller>> controllers();

  /// Custom services
  List<Factory<Service>> services();

}
