import 'package:clean_architecture/src/core/result.dart';

/// A class to send parameter to call a use case
/// Must be extended
/// This was created to improve test driven development
class UseCaseParam {}

/// Usecase represent a class with a single concern.
/// T is the type to be returned
abstract class UseCase<T, Param extends UseCaseParam> {
  T call(Param params);
}

abstract class FutureUseCase<T, Param extends UseCaseParam>
    extends UseCase<Future<Result<T>>, Param> {}

abstract class StreamUseCase<T, Param extends UseCaseParam>
    extends UseCase<Result<Stream<T>>, Param> {}
