import 'package:clean_architecture/src/domain/entity.dart';
import 'package:clean_architecture/src/core/result.dart';

abstract class Repository<EntityType extends Entity, IdType> {}

abstract class CrudRepository<EntityType extends Entity, IdType>
    extends Repository<EntityType, IdType> {
  /// Returns the numbers of entities available
  Future<Result<int>> count();

  /// Deletes a given entity
  Future<Result<bool>> delete(EntityType entity);

  /// Deletes all entities managed by the repository.
  Future<Result<bool>> deleteAll();

  /// Deletes the given entities.
  Future<Result<bool>> deleteEntities(Iterable<EntityType> entities);

  /// Deletes the entity with the given id.
  Future<Result<bool>> deleteById(IdType id);

  /// Returns whether an entity with the given id exists.
  Future<Result<bool>> existsById(IdType id);

  /// Returns all instances of the type.
  Future<Result<Iterable<EntityType>>> findAll();

  /// Returns all instances of the type T with the given IDs.
  Future<Result<Iterable<EntityType>>> findAllById(Iterable<IdType> ids);

  /// Retrieves an entity by its id.
  Future<Result<EntityType>> findById(IdType id);

  /// Saves a given entity.
  Future<Result<EntityType>> save(EntityType entity);

  /// Saves all given entities.
  Future<Result<Iterable<EntityType>>> saveAll(Iterable<EntityType> entities);
}

abstract class StreamRepository<EntityType extends Entity, IdType>
    extends Repository<EntityType, IdType> {
  Result<Stream<EntityType>> streamById(IdType id);

  Result<Stream<List<Entity>>> streamAll();

  void dispose();
  
}
