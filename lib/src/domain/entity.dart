import 'package:clean_architecture/src/core/equals.dart';

/// This class represent an entity.
/// Every entity have an unique id
/// Every entity extends equals and by default the id is used to compare the
/// entity equality. This can be overrided.
class Entity<IdType> extends Equals {
  IdType id;

  Entity({this.id});

  @override
  List<Object> get properties => [id];
}
