library clean_architecture;

/// Core
export 'src/core/clean_architecture.dart';
export 'src/core/configuration.dart';
export 'src/core/environment.dart';
export 'src/core/equals.dart';
export 'src/core/factory.dart';
export 'src/core/implementation.dart';
export 'src/core/result.dart';
export 'src/core/stream_converter.dart';
export 'src/core/service.dart';

/// Domain
export 'src/domain/entity.dart';
export 'src/domain/repository.dart';
export 'src/domain/usecase.dart';

/// Data
export 'src/data/model.dart';

/// Presentations
export 'src/presentation/component.dart';
export 'src/presentation/controller.dart';
export 'src/presentation/view.dart';


